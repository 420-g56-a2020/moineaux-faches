﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GestionnaireAudio : MonoBehaviour
{
	public AudioMixer mixer;

    // Start is called before the first frame update
    void Start()
    {
    	if(PlayerPrefs.HasKey("VolumeGeneral")) {
     		mixer.SetFloat("VolumeGeneral", PlayerPrefs.GetFloat("VolumeGeneral"));
     	}

     	if(PlayerPrefs.HasKey("VolumeMusique")) {
     		mixer.SetFloat("VolumeMusique", PlayerPrefs.GetFloat("VolumeMusique"));	
     	}

     	if(PlayerPrefs.HasKey("VolumeEffets")) {
     		mixer.SetFloat("VolumeEffets", PlayerPrefs.GetFloat("VolumeEffets"));	
     	}
        
    }

}
