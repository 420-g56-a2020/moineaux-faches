﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoucheAnim : MonoBehaviour
{
	[SerializeField]
	float limiteHaut = 100.0f;

	[SerializeField]
	float limiteBas = -100.0f;

	[SerializeField]
	float limiteDroite = 100.0f;

	[SerializeField]
	float limiteGauche = -100.0f;

	[SerializeField]
	Sprite[] sprites;

	float i = 0.0f;
	float j = 0.0f;

	bool limiteHautAtteinte = false;
	bool limiteBasAtteinte = false;
	bool limiteDroiteAtteinte = false;
	bool limiteGaucheAtteinte = false;

	public int spritePerFrame = 6;
	public bool loop = true;
	public bool destroyOnEnd = false;

	private int index = 0;
	private Image image;
	private int frame = 0;

	void Awake() {
		image = GetComponent<Image> ();
	}

    // Start is called before the first frame update
    void Start()
    {
    	Debug.Log(transform.position.x);
    	Debug.Log(transform.position.y);
    	i = limiteGauche;
        j = limiteHaut;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = new Vector2(i, j);

        deplacerHorizontal();
        deplacerVertical();
        animer();

	}

    private void deplacerHorizontal() {
        if (!limiteDroiteAtteinte) {
            i += 2;
            if(i >= limiteDroite) {
                limiteDroiteAtteinte = true;
                limiteGaucheAtteinte = false;
            }
        } else {
            i -= 2;
            if(i <= limiteGauche) {
                limiteGaucheAtteinte = true;
                limiteDroiteAtteinte = false;
            }
        }
    }

    private void deplacerVertical() {
        if (!limiteHautAtteinte) {
            j ++;
            if(j >= limiteHaut) {
                limiteHautAtteinte = true;
                limiteBasAtteinte = false;
            }
        } else {
            j --;
            if(j <= limiteBas) {
                limiteBasAtteinte = true;
                limiteHautAtteinte = false;
            }
        }
    }

    private void animer() {
        if (!loop && index == sprites.Length) return;
        frame ++;
        if (frame < spritePerFrame) return;
        image.sprite = sprites [index];
        frame = 0;
        index ++;
        if (index >= sprites.Length) {
            if (loop) index = 0;
            if (destroyOnEnd) Destroy (gameObject);
        }
    }
}
