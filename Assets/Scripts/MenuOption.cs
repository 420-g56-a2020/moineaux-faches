﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuOption : MonoBehaviour
{
	public AudioMixer mixer;

	public Slider curseurVolGeneral, curseurVolMusique, curseurVolEffets;
	public Text etiquetteVolGeneral, etiquetteVolMusique, etiquetteVolEffets;

	public AudioSource sfx;

    // Start is called before the first frame update
    void Start()
    {

     	if(PlayerPrefs.HasKey("VolumeGeneral")) {
     		mixer.SetFloat("VolumeGeneral", PlayerPrefs.GetFloat("VolumeGeneral"));
     		curseurVolGeneral.value = PlayerPrefs.GetFloat("VolumeGeneral");
     		
     	}

     	if(PlayerPrefs.HasKey("VolumeMusique")) {
     		mixer.SetFloat("VolumeMusique", PlayerPrefs.GetFloat("VolumeMusique"));
     		curseurVolMusique.value = PlayerPrefs.GetFloat("VolumeMusique");
     		
     	}

     	if(PlayerPrefs.HasKey("VolumeEffets")) {
     		mixer.SetFloat("VolumeEffets", PlayerPrefs.GetFloat("VolumeEffets"));
     		curseurVolEffets.value = PlayerPrefs.GetFloat("VolumeEffets");
     		
     	}

     	etiquetteVolGeneral.text = (curseurVolGeneral.value + 80).ToString();
     	etiquetteVolMusique.text = (curseurVolMusique.value + 80).ToString();
     	etiquetteVolEffets.text = (curseurVolEffets.value + 80).ToString();
    }

    public void appliquerVolGeneral() {

    	etiquetteVolGeneral.text = (curseurVolGeneral.value + 80).ToString();
    	mixer.SetFloat("VolumeGeneral", curseurVolGeneral.value);

    	PlayerPrefs.SetFloat("VolumeGeneral", curseurVolGeneral.value);
    }

    public void appliquerVolMusique() {

    	etiquetteVolMusique.text = (curseurVolMusique.value + 80).ToString();
    	mixer.SetFloat("VolumeMusique", curseurVolMusique.value);

    	PlayerPrefs.SetFloat("VolumeMusique", curseurVolMusique.value);
    }

    public void appliquerVolEffets() {
    	etiquetteVolEffets.text = (curseurVolEffets.value + 80).ToString();
    	mixer.SetFloat("VolumeEffets", curseurVolEffets.value);

    	PlayerPrefs.SetFloat("VolumeEffets", curseurVolEffets.value);
    }

    public void jouerSFX() {
    	sfx.Play();
    }

    public void arreterSFX() {
    	sfx.Stop();
    }
}
