﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPause : MonoBehaviour
{
	public GameObject fenetreOption, fenetrePause;
	public string menuPrincipal;

	private bool enPause = false;

	public GameObject fenetreChargement, iconeChargement;
	public Text texteChargement;

    // Start is called before the first frame update
    void Start()
    {
     	fenetreOption.SetActive(false);
     	enleverPause();   
    }

    // Update is called once per frame
    void Update()
    {
    	if(Input.GetKeyDown(KeyCode.Escape)) {
    		enleverMettrePause();
    	}
        
    }

    public void ouvrirOptions() {
        fenetreOption.SetActive(true);
    }

    public void fermerOptions() {
        fenetreOption.SetActive(false);
    }

    public void retournerMenuPrincipal() {
    	//SceneManager.LoadScene(menuPrincipal);
    	enleverPause();

    	StartCoroutine(ChargerPrincipal());
    }

    public void mettreEnPause() {
    	fenetrePause.SetActive(true);
    	enPause = true;

    	Time.timeScale = 0;
    }

    public void enleverPause() {
    	fenetrePause.SetActive(false);
    	enPause = false;

    	Time.timeScale = 1;
    }

    public void enleverMettrePause() {
    	if(!enPause) {
    		mettreEnPause();

    	} else {
    		enleverPause();
    	}
    }

    public IEnumerator ChargerPrincipal() {
        
        fenetreChargement.SetActive(true);

        yield return null;

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(menuPrincipal);
        asyncLoad.allowSceneActivation = false;

        while(!asyncLoad.isDone) {

            if(asyncLoad.progress >= .9f){
                texteChargement.text = "Appuyer sur une touche pour continuer";
                iconeChargement.SetActive(false);

                if(Input.anyKeyDown) {
                    asyncLoad.allowSceneActivation = true;
                }
            }

            yield return null;
        }
    }
}
