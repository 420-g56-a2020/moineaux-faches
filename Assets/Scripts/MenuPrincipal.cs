﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPrincipal : MonoBehaviour
{

	public string niveau1;
    public GameObject fenetreOption;

    public GameObject fenetreChargement, iconeChargement;
    public Text texteChargement;

    // Start is called before the first frame update
    void Start()
    {
        fenetreOption.SetActive(false);
    }

    public void demarrerJeu() {

    	//SceneManager.LoadScene(niveau1);
        StartCoroutine("ChargerNiveau");
    }

    public void ouvrirOptions() {
        fenetreOption.SetActive(true);
    }

    public void fermerOptions() {
        fenetreOption.SetActive(false);
    }

    public void quitterJeu() {

    	Application.Quit();
    }

    public IEnumerator ChargerNiveau() {
        
        fenetreChargement.SetActive(true);

        yield return null;

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(niveau1);
        asyncLoad.allowSceneActivation = false;

        while(!asyncLoad.isDone) {

            if(asyncLoad.progress >= .9f){
                texteChargement.text = "Appuyer sur une touche pour continuer";
                iconeChargement.SetActive(false);

                if(Input.anyKeyDown) {
                    asyncLoad.allowSceneActivation = true;
                }
            }

            yield return null;
        }
    }
}
